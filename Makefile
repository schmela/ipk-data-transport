CC=g++
CPPFLAGS=-std=c++98 -pedantic -Wextra -g

all: rdtclient rdtserver

rdtclient: rdtclient.cpp rdtclient.hpp
	$(CC) rdtclient.cpp $(CPPFLAGS) -o rdtclient

rdtserver: rdtserver.cpp rdtserver.hpp
	$(CC) rdtserver.cpp $(CPPFLAGS) -o rdtserver

pack: 
	tar -czf xhrade08.tar.gz Makefile rdtserver.cpp rdtserver.hpp rdtclient.cpp rdtclient.hpp udt.h base64.cpp base64.h readme.xml
