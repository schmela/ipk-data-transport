#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include <signal.h>

#include "rdtclient.hpp"
#include "udt.h"
#include "base64.cpp"

using namespace std;

const int MAXLINE = 80;
const int MAXLENGTH = 512;
const int WINDOW_SIZE = 5;
const int TIMEOUT = 5;

std::vector<std::string> window(WINDOW_SIZE); // slide window
std::vector<std::string> buffer(0); // k ukladani ze stdin

int next_seq = 1;
int base = 1;
int seq = 1;

int udt;

// porty a adresy
in_port_t local_port;
in_port_t remote_port;
in_addr_t remote_addr = 0x7f000001; // localhost

// timer a signaly
struct itimerval timer;
sigset_t sigmask;


std::string IntToStr(int tmp)
{
	std::ostringstream out;
	out << tmp;
	return out.str();
}

// zpracovani parametru
TParams::TParams(int argc,char *argv[])
{
	if (argc==2) {
		if (strcmp (argv[1],"-h") == 0)
		{
			cout <<	"CLIENT - preklad domenovych jmen na IPv4 a IPv6 adresy"<<endl<<
					"pouziti:"<<endl<<
					"client -h vypise tuto napovedu"<<endl<<
					"client [adresa_serveru:port] -46 [domenove_jmeno]"<<endl<<
					"na adrese_serveru musi na danem portu naslouchat serverova cast aplikace"<<endl<<
					"-4 prelozi zadane domenove jmeno na IPv4 adresu"<<endl<<
					"-6 prelozi zadane domenove jmeno na IPv6 adresu"<<endl<<flush;
			exit(EXIT_SUCCESS);
		}
		else
		{
			throw badParameters();
		}
	} 
	else {
		int s=0,d=0; // pocty kolikrat ktery argumemt je
		if (argc!=5) throw badParameters();
		for (int i=1; i<argc-1; i++)
		{
			if ((strcmp(argv[i],"-s")==0)&&(s++==0))
			{
				setSourcePort(atoi(argv[i+1]));
				i++;
			}
			else if ((strcmp(argv[i],"-d")==0)&&(d++==0)) 
			{
				setDestinationPort(atoi(argv[i+1]));
				i++;
			}
			else
				{throw badParameters();}

		}		
	}
}

void TParams::setSourcePort(int aPort)
{
	sourcePort = aPort;
}

void TParams::setDestinationPort(int aPort)
{
	destinationPort = aPort;
}

int TParams::getSourcePort()
{
	return sourcePort;
}

int TParams::getDestinationPort()
{
	return destinationPort;
}

// vrati obsah prvni zavorky z regexpu, kdyz neni nalezena, vraci se ""
string regexp(string source, string pattern)
{
    regex_t regex;
	int c;

	// kompilace regulerniho vyrazu
	try
	{
    	c = regcomp(&regex, pattern.c_str(), REG_EXTENDED);
		if ( c != 0 ) {throw regexpCompilationError();}
	}
	catch (regexpCompilationError) 
	{
		cerr << "Regex compilation Error" << endl;
		regfree(&regex);
		exit ( EXIT_FAILURE );
	}

	// vykonani regulerniho vyrazu
	size_t     nmatch = 2;
	regmatch_t pmatch[2];
	if ((regexec(&regex,source.c_str(),nmatch,pmatch,0)) == 0)
	{
		int begin = (int)pmatch[1].rm_so;
		int end = (int)pmatch[1].rm_eo;
		int length = end-begin;
		regfree(&regex);
		if ((begin==-1)||(end==-1)) return "";
		else return source.substr(begin,length); 
	}
	else
	{
		// kdyz se nic nenaslo, vraci se prazdny retezec
		regfree(&regex);
		return "";
	}
}

// vynulovani timeru a zruseni maskovani signalu SIGALRM
void startTimer()
{
	setitimer(ITIMER_REAL, &timer, NULL);
	sigprocmask(SIG_UNBLOCK, &sigmask, NULL);
}

// zastaveni timeru vymaskovanim signalu SIGALRM
void stopTimer()
{
	sigprocmask(SIG_BLOCK, &sigmask, NULL);
}

// pri vyprseni casovace se musi vsechny packety ve slide window poslat znova
void signal_alarm(int sig)
{
	cout << "Alarm recieved:" << endl << flush;

	// znovu se spusti casovac
	startTimer();

	// vsechny pakety ve slide window se poslou znovu
	for(int i = base; i <= next_seq ; i++)
  	{
		udt_send(udt, remote_addr, remote_port, (void*)window[i % WINDOW_SIZE].c_str(), window[i % WINDOW_SIZE].length());
		cout << ">>> " << window[i % WINDOW_SIZE] << endl <<flush;
	}

	signal(SIGALRM, signal_alarm);
}

void endConnection(int seqnumber)
{

	// vytvoreni packetu ve formatu xml
	string packet;
	packet.append("<rdt-segment id=xhrade08>");
	packet.append("<header>");
	packet.append("0 "); // ACK=0
	packet.append(IntToStr(seqnumber)); // cislo sequence
	packet.append(" ");
	packet.append("1");
	packet.append("</header>");
	packet.append("<data>");
	packet.append("END"); // znaci koncovy segment
	packet.append("</data>");
	packet.append("</rdt-segment>");
	packet.append("\0");

	// poslani packetu serveru
	udt_send(udt, remote_addr, remote_port, (void*)packet.c_str(), packet.length());
	cout << ">>> " << packet << endl << flush;
	return;
}
string createPacket(int seqnumber, char data[80],int pocet,bool last)
{
	// prevedeni dat do base64
	//unsigned char*data_;
	//memcpy(data_,80,)
	string encoded_data = base64_encode((const unsigned char*)data,pocet);
	//string encoded_data = base64_encode(reinterpret_cast<const unsigned char*>(data.c_str()),pocet);

	// vytvoreni packetu ve formatu xml
	string packet;
	packet.append("<rdt-segment id=xhrade08>");
	packet.append("<header>");
	packet.append("0 "); // ACK=0
	packet.append(IntToStr(seqnumber)); // cislo sequence
	packet.append(" ");
	packet.append((last)? "1" : "0");
	packet.append("</header>");
	packet.append("<data>");
	packet.append(encoded_data); // zakodovana base64 data
	packet.append("</data>");
	packet.append("</rdt-segment>");
	packet.append("\0");

	return packet;
}

int main(int argc,char *argv[]) 
{
	try
	{
		// zpracovani parametru
		TParams parametry(argc,argv);

		// nastaveni timeru
		timer.it_interval.tv_sec = TIMEOUT;
		timer.it_interval.tv_usec = 0;
		timer.it_value.tv_sec = TIMEOUT;
		timer.it_value.tv_usec = 0;

		// odchytavani signalu SIGALRM		
		signal(SIGALRM, signal_alarm);

		// pomocne buffery
		char buf[80];
		char sendline[MAXLINE];
		char recvline[MAXLINE];
		int udt;

		// porty a adresy
		in_port_t local_port = parametry.getSourcePort();
		in_port_t remote_port = parametry.getDestinationPort();
		in_addr_t remote_addr = 0x7f000001; // localhost
		
		//inicializace UDT
		udt = udt_init(local_port);
		
		fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);	// make stdin reading non-clocking

		fd_set readfds;
		FD_ZERO(&readfds);
		FD_SET(udt, &readfds);
		FD_SET(STDIN_FILENO, &readfds);

		freopen(NULL, "rb", stdin);

		while (select(udt+1, &readfds, NULL, NULL, NULL)) 
		{
			// nacetla se nova radka ze stdin - je treba ji zpracovat
			if (FD_ISSET(STDIN_FILENO, &readfds)) 
			{
				if (next_seq < (base + WINDOW_SIZE)) // kdyz neni plne okno
				{
					//fgets(sendline, MAXLINE, stdin);
					//string li
					
					if (! feof(stdin))
					{
						int pocet = fread(sendline, 1, MAXLINE-1, stdin);
						sendline[MAXLINE]='\0';
						cerr << pocet << endl << flush;

						//if (feof(stdin))
							//cout << "konec souboru, next_seq= "<< next_seq <<endl <<flush;
						
						// vytvori packet a uloz ho do okna
						window[next_seq % WINDOW_SIZE] = createPacket(next_seq,sendline,pocet,(feof(stdin))? true : false);

						// odesleme vsechny packety v okne
						for (int i=base; i<= next_seq; i++)
						{
							if (!udt_send(udt, remote_addr, remote_port, (void *)window[i % WINDOW_SIZE].c_str(), window[i % WINDOW_SIZE].length())) 
							{
								cerr << "UDT: chyba pri odesilani" << endl << flush; 
								exit(EXIT_FAILURE);
							}
							//cout << ">   " << window[i % WINDOW_SIZE] << endl<<flush;
						}	

						// spusti se timer
						startTimer();

						next_seq++;
					}
				}
			}

			// prisla nam nova zprava - je ji treba zpracovat
			if (FD_ISSET(udt, &readfds)) 
			{

				//cout << "prisla zprava" <<endl<<flush;
				int n = udt_recv(udt, recvline, MAXLENGTH, NULL, NULL);
				recvline[n] = 0;
				//fputs(recvline, stdout);
				//cout <<  "<<< " <<recvline << endl<<flush;

				string str_sequence = regexp(recvline,"<header>[[:digit:]]+ (.*) [[:digit:]]+</header>");
				if (str_sequence=="") continue;
					else seq = atoi(str_sequence.c_str());
				//cout << "Recieved ACK for sequence " << seq <<endl<<flush;



				// kdyz dorazilo potvrzeni prvniho packetu ze sliding window
				if (seq >= base)
				{
					// posuneme okno 1 
					stopTimer();
					//base++;
					
					if ( ((seq+1)-base) > 4 )
						base = seq;
					else
						base = seq+1;
						
					//cout << "Accepted ACK for sequence " << seq <<endl<<flush;

					if (base == next_seq)
					{
						endConnection(next_seq);
						return(0);
					}
				}
				else if (seq<base)
				{
					// odesleme vsechny packety v okne
					for (int i=base; i<= next_seq; i++)
					{
						if (!udt_send(udt, remote_addr, remote_port, (void *)window[i % WINDOW_SIZE].c_str(), window[i % WINDOW_SIZE].length())) 
						{
							cerr << "UDT: chyba pri odesilani" << endl << flush; 
							exit(EXIT_FAILURE);
						}
						startTimer();
						//cout << ">>  " << window[i % WINDOW_SIZE] << endl << flush;
					}	
					
				}
			

			}

			// znovunastaveni readfds
			FD_ZERO(&readfds);
			FD_SET(udt, &readfds);
			FD_SET(STDIN_FILENO, &readfds);
		}
		
	}

	catch (TParams::badParameters)
	{
		cerr << "Spatne parametry" << endl;
		return EXIT_FAILURE;
	}
	
	catch (TParams::errorMissingPort)
	{
		cerr << "Chybejici informace o portu" << endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
