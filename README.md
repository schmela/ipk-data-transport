# IPK Data transport #

Projekt 3 do předmětu IPK
Safe data transport using UDP protocol.

### About ###

* Úkolem bylo vytvořit server-client aplikaci, která bude přenášet data pomocí protokolu UDP tak, že přenos bude efektivně fungovat v prostředí, kde se ztrácí velké množství paketů.