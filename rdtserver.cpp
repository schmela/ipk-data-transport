#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <iterator>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include <signal.h>

#include "rdtserver.hpp"
#include "udt.h"
#include "base64.cpp"

using namespace std;

const int MAXLINE = 80;
const int MAXLENGTH = 512;
const int WINDOW_SIZE = 5;
const int TIMEOUT = 5;

std::vector<int> window(WINDOW_SIZE); // slide window
std::vector<std::string> buffer(WINDOW_SIZE); // k ukladani ze stdin

int next_seq = 1;
int base = 1;
int seq = 1;

int udt;

// porty a adresy
in_port_t local_port;
in_port_t remote_port;
in_addr_t remote_addr = 0x7f000001; // localhost

std::string IntToStr(int tmp)
{
	std::ostringstream out;
	out << tmp;
	return out.str();
}

// zpracovani parametru
TParams::TParams(int argc,char *argv[])
{
	if (argc==2) {
		if (strcmp (argv[1],"-h") == 0)
		{
			cout <<	"CLIENT - preklad domenovych jmen na IPv4 a IPv6 adresy"<<endl<<
					"pouziti:"<<endl<<
					"client -h vypise tuto napovedu"<<endl<<
					"client [adresa_serveru:port] -46 [domenove_jmeno]"<<endl<<
					"na adrese_serveru musi na danem portu naslouchat serverova cast aplikace"<<endl<<
					"-4 prelozi zadane domenove jmeno na IPv4 adresu"<<endl<<
					"-6 prelozi zadane domenove jmeno na IPv6 adresu"<<endl<<flush;
			exit(EXIT_SUCCESS);
		}
		else
		{
			throw badParameters();
		}
	} 
	else {
		int s=0,d=0; // pocty kolikrat ktery argumemt je
		if (argc!=5) throw badParameters();
		for (int i=1; i<argc-1; i++)
		{
			if ((strcmp(argv[i],"-s")==0)&&(s++==0))
			{
				setSourcePort(atoi(argv[i+1]));
				i++;
			}
			else if ((strcmp(argv[i],"-d")==0)&&(d++==0)) 
			{
				setDestinationPort(atoi(argv[i+1]));
				i++;
			}
			else
				{throw badParameters();}

		}		
	}
}

void TParams::setSourcePort(int aPort)
{
	sourcePort = aPort;
}

void TParams::setDestinationPort(int aPort)
{
	destinationPort = aPort;
}

int TParams::getSourcePort()
{
	return sourcePort;
}

int TParams::getDestinationPort()
{
	return destinationPort;
}

// vrati obsah prvni zavorky z regexpu, kdyz neni nalezena, vraci se ""
string regexp(string source, string pattern)
{
    regex_t regex;
	int c;

	// kompilace regulerniho vyrazu
	try
	{
    	c = regcomp(&regex, pattern.c_str(), REG_EXTENDED);
		if ( c != 0 ) {throw regexpCompilationError();}
	}
	catch (regexpCompilationError) 
	{
		cerr << "Regex compilation Error" << endl;
		regfree(&regex);
		exit ( EXIT_FAILURE );
	}

	// vykonani regulerniho vyrazu
	size_t     nmatch = 2;
	regmatch_t pmatch[2];
	if ((regexec(&regex,source.c_str(),nmatch,pmatch,0)) == 0)
	{
		int begin = (int)pmatch[1].rm_so;
		int end = (int)pmatch[1].rm_eo;
		int length = end-begin;
		regfree(&regex);
		if ((begin==-1)||(end==-1)) return "";
		else return source.substr(begin,length); 
	}
	else
	{
		// kdyz se nic nenaslo, vraci se prazdny retezec
		regfree(&regex);
		return "";
	}
}

string createACKPacket(int seqnumber)
{
	// vytvoreni packetu ve formatu xml
	string packet;
	packet.append("<rdt-segment id=xhrade08>");
	packet.append("<header>");
	packet.append("1 "); // ACK=0
	packet.append(IntToStr(seqnumber)); // cislo sequence
	packet.append(" 0");
	packet.append("</header>");
	packet.append("<data>");
	// datova cast je prazdna
	packet.append("</data>");
	packet.append("</rdt-segment>");
	packet.append("\0");

	return packet;
}

int main(int argc,char *argv[]) 
{
	try
	{
		// zpracovani parametru
		TParams parametry(argc,argv);

		// pomocne buffery
		char buf[80];
		char sendline[MAXLENGTH];
		char recvline[MAXLENGTH];
		int udt;
		int expected = 1;
		//int end=0;
		//int end_soucet = -1;
		//int printed = 0;

		// porty a adresy
		in_port_t local_port = parametry.getSourcePort();
		in_port_t remote_port = parametry.getDestinationPort();
		in_addr_t remote_addr = 0x7f000001; // localhost
		
		//inicializace UDT
		udt = udt_init(local_port);
		
		fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);	// make stdin reading non-clocking

		fd_set readfds;
		FD_ZERO(&readfds);
		FD_SET(udt, &readfds);
		FD_SET(STDIN_FILENO, &readfds);

		freopen(NULL,"wb",stdout);

		while (select(udt+1, &readfds, NULL, NULL, NULL)) 
		{
			// prisla nam nova zprava - je ji treba zpracovat
			if (FD_ISSET(udt, &readfds)) 
			{
				// prijmuti a zakladni zpracovani zpravy
				int n = udt_recv(udt, recvline, MAXLENGTH, NULL, NULL);
				recvline[n] = 0;
				//cerr << "<<< " << recvline << endl<<flush;
				string data = regexp(recvline,"<data>(.*)</data>");

				// vykuchani cisla sequence z pridatych dat
				int sequence;
				string str_sequence = regexp(recvline,"<header>[[:digit:]]+ (.*) [[:digit:]]+</header>");
				if (str_sequence=="") continue;
				else sequence = atoi(str_sequence.c_str());

				string decoded;
				if (sequence==expected)
				{
					decoded = base64_decode(data);
					char dec[MAXLINE];
					memcpy(dec,decoded.c_str(),decoded.length()) ;
					//cerr << "decoded" << decoded << endl << flush;
					//fwrite(decoded.c_str(),1,decoded.length(),stdout);
					fwrite(dec,1,decoded.length(),stdout);
					fflush(stdout);
					expected++;

					string ackpacket = createACKPacket(sequence); 
					if (!udt_send(udt, remote_addr, remote_port, (void *)ackpacket.c_str(), ackpacket.length())) 
					{
						cerr << "UDT: chyba pri odesilani" << endl << flush; 
						exit(EXIT_FAILURE);
					}
					//cerr << ">   poslano ACK pro " << sequence << endl <<flush;

				}

				if (data=="END")
				{
					//cerr << "kliet skoncil" << endl << flush;

				}


			}

			// znovunastaveni readfds
			FD_ZERO(&readfds);
			FD_SET(udt, &readfds);
			FD_SET(STDIN_FILENO, &readfds);
		}

		//cout << " konec " << endl << flush;
		
	}

	catch (TParams::badParameters)
	{
		cerr << "Spatne parametry" << endl;
		return EXIT_FAILURE;
	}
	
	catch (TParams::errorMissingPort)
	{
		cerr << "Chybejici informace o portu" << endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
