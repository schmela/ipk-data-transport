#include <iostream>
#include <string>

using namespace std;

class TParams
{
	private:
		int sourcePort; // port na kterem posloucha klient
		int destinationPort; // port na kterem posloucha server

		void setSourcePort(int aPort);
		void setDestinationPort(int aPort);

	public:
		TParams(int argc,char *argv[]);
		int getSourcePort();
		int getDestinationPort();
		
		class badParameters{};
		class errorMissingPort{};
};

class getaddrinfoError{};
class socketError{};
class connectError{};
class writeError{};
class readError{};
class closeError{};        

string regexp(string source, string pattern);
class regexpCompilationError{};
